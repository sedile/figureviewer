#include "VCircle.h"

VCircle::VCircle()
    : AbstractFigure(),
    _point(VPoint()), _radius(0) {}

VCircle::VCircle(const VPoint &p, const tNatZahl &r)
    : AbstractFigure(),
    _point(p), _radius(r) {}

VCircle::VCircle(const VCircle &c)
    : AbstractFigure(),
    _point(c._point), _radius(c._radius) {}

VCircle::~VCircle() {}

VCircle* VCircle::getCircle() const {
    return new VCircle(_point, _radius);
}

tNatZahl VCircle::getRadius() const {
    return _radius;
}

std::string VCircle::getFigureName() const {
    return "VCircle";
}

std::vector<tNatZahl> VCircle::getCoords() const {
    std::vector<tNatZahl> coord;
    coord.push_back(_point.getX());
    coord.push_back(_point.getY());
    coord.push_back(_radius);
    return coord;
}

std::string VCircle::toString() const {
    return "( circle(" + _point.toString() +") )";
}
