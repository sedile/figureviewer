#ifndef STARTWINDOW_H
#define STARTWINDOW_H

#include <QMainWindow>
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QToolBar>
#include <QStatusBar>
#include "VControler.h"

/* VorwÃ¤rtsdeklaration : #include wird in .cpp-Datei geschrieben, da .h nicht zwingend
 * eine abhÃ¤ngigkeit haben muss */
class FigureCreator;

class StartWindow : public QMainWindow {
private:
    Q_OBJECT

    VControler *_control;
    FigureCreator *_figurecreator;

    QMenu *_windowMenu;
    QMenu *_chooserMenu;
    QAction *_close;
    QAction *_painter;
    QAction *_coord;
    QToolBar *_tb;

    void initComponents();
    void initLayout();
    void initConnections();

public:
    explicit StartWindow(VControler *control, QWidget *parent = 0);
    virtual ~StartWindow();

private slots:
    void openPainter();
    void openCoord();
    void closeApp();
};

#endif // STARTWINDOW_H
