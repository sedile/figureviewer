#include "Canvas.h"
#include <QPainter>
#include <QString>
#include <QMessageBox>

#include "VPoint.h"

Canvas::Canvas(QWidget *parent) : QWidget(parent), _single(true), _figure(nullptr) {
    setFixedSize(768,768);
}

Canvas::~Canvas() {
    delete _painter;
}

void Canvas::setBackground(){
    QPalette pal;
    pal.setColor(QPalette::Background, Qt::white);
    setAutoFillBackground(true);
    setPalette(pal);
}

void Canvas::setFigure(AbstractFigure *f){
    _figure = f;
}

void Canvas::setFigureList(std::vector<AbstractFigure *> flist){
    _figureList = flist;
}

void Canvas::setSingleFlag(const bool &flag){
    _single = flag;
}

void Canvas::drawFigure(){

    if( _figure->getFigureName() == "VLine" ){
        drawVLine(_figure->getCoords());
    } else if ( _figure->getFigureName() == "VCircle" ){
        drawVCircle(_figure->getCoords());
    } else if ( _figure->getFigureName() == "VTriangle" ){
        drawVTriangle(_figure->getCoords());
    } else if ( _figure->getFigureName() == "VRect" ){
        drawVRectangle(_figure->getCoords());
    }
}

void Canvas::drawFigure(const AbstractFigure &f){

    if( f.getFigureName() == "VLine" ){
        drawVLine(f.getCoords());
    } else if ( f.getFigureName() == "VCircle" ){
        drawVCircle(f.getCoords());
    } else if ( f.getFigureName() == "VTriangle" ){
        drawVTriangle(f.getCoords());
    } else if ( f.getFigureName() == "VRect" ){
        drawVRectangle(f.getCoords());
    }
}

void Canvas::drawAllFigures(){
    for(tNatZahl i = 0; i < _figureList.size(); i++){
        drawFigure(*_figureList.at(i));
    }
}

void Canvas::drawVLine(const std::vector<tNatZahl> &coord){
    _painter->drawLine(coord.at(0),coord.at(1),coord.at(2),coord.at(3));
}

void Canvas::drawVCircle(const std::vector<tNatZahl> &coord){
    _painter->drawEllipse(coord.at(0),coord.at(1),coord.at(2),coord.at(2));
}

void Canvas::drawVTriangle(const std::vector<tNatZahl> &coord){
    _painter->drawLine(coord.at(0),coord.at(1),coord.at(2),coord.at(3));
    _painter->drawLine(coord.at(2),coord.at(3),coord.at(4),coord.at(5));
    _painter->drawLine(coord.at(4),coord.at(5),coord.at(0),coord.at(1));
}

void Canvas::drawVRectangle(const std::vector<tNatZahl> &coord){
    _painter->drawLine(coord.at(0),coord.at(1),coord.at(2),coord.at(3));
    _painter->drawLine(coord.at(2),coord.at(3),coord.at(4),coord.at(5));
    _painter->drawLine(coord.at(4),coord.at(5),coord.at(6),coord.at(7));
    _painter->drawLine(coord.at(6),coord.at(7),coord.at(0),coord.at(1));
}

void Canvas::paintEvent(QPaintEvent *){
    _painter = new QPainter(this);
    _painter->setPen(Qt::red);

    if( _single ){
        drawFigure();
    } else {
        drawAllFigures();
    }
}

void Canvas::mousePressEvent(QMouseEvent *event){

    QString info;

    if( event->button() == Qt::LeftButton ){
        info.append("Links ");
    } else if ( event->button() == Qt::MidButton ){
        info.append("Mitte ");
    } else if ( event->button() == Qt::RightButton ){
        info.append("Rechts ");
    }

    QString posx = "("+QString::number(event->x())+" ";
    QString posy = QString::number(event->y())+")";

    info.append(posx);
    info.append(posy);

    QMessageBox::information(this, tr("Info"), info);
}
