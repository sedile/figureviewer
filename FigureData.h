#ifndef FIGUREDATA_H
#define FIGUREDATA_H

#include <QTextEdit>
#include <QComboBox>

#include "VControler.h"

class FigureData : public QWidget {
private:
    Q_OBJECT

    VControler *_control;
    QComboBox *_figureTypes;
    QTextEdit *_figureNL;

    void initComponents();
    void initSlotSignalConnection();
    void initLayout();

public:
    explicit FigureData(VControler *controler, QWidget *parent = nullptr);
    virtual ~FigureData();

private slots:
    void updateTextArea();
};

#endif // FIGUREDATA_H
