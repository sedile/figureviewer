#ifndef VLINE_H_INCLUDED
#define VLINE_H_INCLUDED

#include "AbstractFigure.h"
#include "VPoint.h"

class VLine : public AbstractFigure {
private:
    const VPoint _start, _end;
public:
    explicit VLine();
    explicit VLine(const VPoint &start, const VPoint &end);
    explicit VLine(const VLine &l);
    virtual ~VLine();

    VPoint* getStartPoint() const;
    VPoint* getEndPoint() const;

    std::string getFigureName() const override;
    std::vector<tNatZahl> getCoords() const override;
    std::string toString() const override;
};

#endif // VLINE_H_INCLUDED
