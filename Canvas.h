#ifndef CANVAS_H
#define CANVAS_H

#include <QWidget>
#include <QMouseEvent>
#include <vector>

#include "AbstractFigure.h"

class Canvas : public QWidget {
private:
    Q_OBJECT

    QPainter *_painter;
    AbstractFigure *_figure;
    std::vector<AbstractFigure*> _figureList;
    bool _single;

    void drawFigure();
    void drawFigure(const AbstractFigure &f);
    void drawAllFigures();
    void drawVLine(const std::vector<tNatZahl> &coord);
    void drawVCircle(const std::vector<tNatZahl> &coord);
    void drawVTriangle(const std::vector<tNatZahl> &coord);
    void drawVRectangle(const std::vector<tNatZahl> &coord);

public:
    explicit Canvas(QWidget *parent = nullptr);
    virtual ~Canvas();

    void setBackground();
    void setFigure(AbstractFigure *f);
    void setFigureList(std::vector<AbstractFigure*> flist);
    void setSingleFlag(const bool &flag);

protected:
    void paintEvent(QPaintEvent *);
    void mousePressEvent(QMouseEvent *event);
};

#endif // CANVAS_H
