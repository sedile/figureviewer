#include "VControler.h"
#include "VPoint.h"
#include "VLine.h"
#include "VRect.h"
#include "VTriangle.h"
#include "VCircle.h"

VControler::VControler(){ }

VControler::~VControler(){ }

bool VControler::createFigure(const tNatZahl &amount, const char &name){
    VPoint *dummy = new VPoint;

        switch(name){
        case 'L': {
            VPoint *p = dummy->generateVPoints(2 * amount);
            for(tNatZahl i = 0; i < 2 * amount; i += 2){
                VLine *l = new VLine(*(p + i), *(p + i + 1));
                _figurelist.push_back(l);
            }
            break;
            }
        case 'C': {
            VPoint *p = dummy->generateVPoints(amount);
            for(tNatZahl i = 0; i < amount; i++){
                VCircle *c = new VCircle(*(p + i), 32);
                _figurelist.push_back(c);
            }
            break;
            }
        case 'T': {
            VPoint *p = dummy->generateVPoints(3 * amount);
            for(tNatZahl i = 0; i < 3 * amount; i += 3){
                VTriangle *t = new VTriangle(*(p + i), *(p + i + 1), *(p + i + 2));
                _figurelist.push_back(t);
            }
            break;
            }
        case 'R': {
            VPoint *p = dummy->generateVPoints(4 * amount);
            for(tNatZahl i = 0; i < 4 * amount; i += 4){
                VRect *r = new VRect(*(p + i), *(p + i + 1), *(p + i + 2), *(p + i + 3));
                _figurelist.push_back(r);
            }
            break;
            }
        default:
            return false;
        }

    delete dummy;
    dummy = nullptr;
    return true;
}

bool VControler::deleteFigure(const tNatZahl &idx){
    if ( idx > _figurelist.size() ){
        return false;
    }

    _figurelist.erase(_figurelist.begin() + idx);
    return true;
}

void VControler::clearFigureList(){
    _figurelist.clear();
}

std::vector<std::string> VControler::extractPoints(const std::string &fs){
    std::string temp = fs;
    tNatZahl brackets = 0;
    tNatZahl startcut = 0;
    for(tNatZahl i = 0; i < fs.length(); i++){
        if( fs.at(i) == '(' ){
            brackets++;
            if ( brackets == 2){
                startcut = i + 1;
                break;
            }
        }
    }

    temp.erase(0, startcut);
    temp.erase(temp.end() - 3, temp.end());

    tNatZahl cap = 0;
    for(tNatZahl j = 0; j < temp.length(); j++){
        if( temp.at(j) == '(' ){
            cap++;
        }
    }

    cap = cap / 2;
    size_t f = -1;
    std::vector<std::string> pointList;
    for(tNatZahl e = 0; e < cap - 1; e++){
        f = temp.find(") (");
        std::string res = temp.substr(0, f+1);
        temp.erase(0, f+2);
        pointList.push_back(res);
        //temp.replace(f, std::string(") (").length(), "),(");
    }

    pointList.push_back(temp);
    return pointList;
}

std::vector<std::string> VControler::showPointsFromFigure(const tNatZahl &idx) {
    if ( idx > _figurelist.size() ){
        std::vector<std::string> pointlist;
        pointlist.push_back("()");
        return pointlist;
    }
    std::vector<std::string> pointlist = extractPoints(_figurelist.at(idx)->toString());;
    return pointlist;
}

AbstractFigure* VControler::getFigure(const tNatZahl &idx) const {
    if ( idx > _figurelist.size() ){
        return new VPoint;
    }
    return _figurelist.at(idx);
}

std::vector<AbstractFigure*> VControler::getFigureList() const {
    return _figurelist;
}

std::string* VControler::nestedListFromFigures() const {
    std::string *nl = new std::string[_figurelist.size()];

    for(tNatZahl i = 0; i < _figurelist.size(); i++){
        *(nl + i) = _figurelist.at(i)->toString();
    }
    return nl;
}
