#ifndef ABSTRACTFIGURE_H_INCLUDED
#define ABSTRACTFIGURE_H_INCLUDED

#include <string>
#include <vector>

typedef unsigned short tNatZahl;

class AbstractFigure {
public:
    virtual ~AbstractFigure() { };

    virtual std::string getFigureName() const = 0;
    virtual std::vector<tNatZahl> getCoords() const = 0;
    virtual std::string toString() const = 0;
};

#endif // ABSTRACTFIGURE_H_INCLUDED
