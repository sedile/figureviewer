#ifndef VPOINT_H_INCLUDED
#define VPOINT_H_INCLUDED

#include "AbstractFigure.h"

class VPoint : public AbstractFigure {
private:
    tNatZahl _x, _y;
public:
    explicit VPoint();
    explicit VPoint(const tNatZahl x, const tNatZahl y);
    explicit VPoint(const VPoint &p);
    virtual ~VPoint();

    VPoint* generateVPoints(const tNatZahl &amount);
    tNatZahl getX() const;
    tNatZahl getY() const;

    std::string getFigureName() const override;
    std::vector<tNatZahl> getCoords() const override;
    std::string toString() const override;
};

#endif // VPOINT_H_INCLUDED
