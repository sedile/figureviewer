#include "VRect.h"

VRect::VRect()
    : AbstractFigure(),
    _ul(VPoint()), _ur(VPoint()), _ll(VPoint()), _lr(VPoint()) { }

VRect::VRect(const VPoint &ul, const VPoint &ur, const VPoint &ll, const VPoint &lr)
    : AbstractFigure(),
    _ul(ul), _ur(ur), _ll(ll), _lr(lr) { }

VRect::VRect(const VRect &r)
    : AbstractFigure(),
    _ul(r._ul), _ur(r._ur), _ll(r._ll), _lr(r._lr) { }

VRect::~VRect() {}

VRect* VRect::getRect() const {
    VRect *rect = new VRect(_ul,_ur,_ll,_lr);
    return rect;
}

std::string VRect::getFigureName() const {
    return "VRect";
}

std::vector<tNatZahl> VRect::getCoords() const {
    std::vector<tNatZahl> coord;
    coord.push_back(_ul.getX());
    coord.push_back(_ul.getY());
    coord.push_back(_ur.getX());
    coord.push_back(_ur.getY());
    coord.push_back(_ll.getX());
    coord.push_back(_ll.getY());
    coord.push_back(_lr.getX());
    coord.push_back(_lr.getY());
    return coord;
}

std::string VRect::toString() const {
    return "( rect(" + _ul.toString() + " " + _ur.toString() + " " + _ll.toString() + " " + _lr.toString() + ") )";
}
