#ifndef FIGUREPAINTER_H
#define FIGUREPAINTER_H

#include <QListWidget>
#include <QPushButton>
#include "Canvas.h"
#include "VControler.h"

class FigurePainter : public QWidget {
private:
    Q_OBJECT

    QListWidget *_figures;
    QPushButton *_drawFigure, *_drawAll;
    Canvas *_canvas;
    VControler *_control;

    void initComponents();
    void initSlotSignalConeection();
    void initLayout();
    void fillListWithFigures();

public:
    explicit FigurePainter(VControler *control, QWidget *parent = nullptr);
    virtual ~FigurePainter();

private slots:
    void drawOneFigure();
    void drawAllFigures();
};

#endif // FIGUREPAINTER_H
