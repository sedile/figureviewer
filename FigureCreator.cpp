#include "FigureCreator.h"
#include "FigureData.h"
#include "FigurePainter.h"

#include <string>

#include <QBoxLayout>
#include <QGroupBox>
#include <QMessageBox>
#include <QFileDialog>
#include <QTextStream>

FigureCreator::FigureCreator(VControler *control, QWidget *parent) : QWidget(parent), _control(control) {
    initComponents();
    initSlotSignalConnection();
    initLayout();
}

FigureCreator::~FigureCreator() {
    delete _control;
    delete _lineBtn;
    delete _circleBtn;
    delete _triBtn;
    delete _rectBtn;
    delete _amountTxt;
    delete _amount;
    delete _confirm;
    delete _delOne;
    delete _delAll;
    delete _coordBtn;
    delete _viewBtn;
    delete _figList;
    delete _saveBtn;
    delete _quitBtn;
}

void FigureCreator::initComponents(){
    _lineBtn = new QRadioButton(tr("&Linie"));
    _lineBtn->setStatusTip("Erzeuge eine neue Linie aus zwei Punkten");
    _circleBtn = new QRadioButton(tr("&Kreis"));
    _circleBtn->setStatusTip("Erzeuge ein Kreis aus ein Mittelpunkt");
    _triBtn = new QRadioButton(tr("&Dreieck"));
    _triBtn->setStatusTip("Erzeuge ein Dreieck aus drei Punkten");
    _rectBtn = new QRadioButton(tr("&Viereck"));
    _rectBtn->setStatusTip("Erzeuge ein Viereck aus vier Punkten");

    _amountTxt = new QLabel(tr("Anzahl der Figuren :"));
    _amount = new QLineEdit;
    _confirm = new QPushButton(tr("Generieren"));
    _confirm->setStatusTip("DrÃ¼cken, um die Anzahl der Figuren zu generieren");

    _delOne = new QPushButton(tr("Loesche Figur"));
    _delOne->setStatusTip("Entfernt die aus der Liste ausgewÃ¤hlte Figur");
    _delAll = new QPushButton(tr("Loesche Liste"));
    _delAll->setStatusTip("Leert die Liste");
    _coordBtn = new QPushButton(tr("Zeige Koordinaten"));
    _coordBtn->setStatusTip("Erzeugt ein neues Fenster, um sich die Koordinaten der Figuren anzusehen");
    _viewBtn = new QPushButton(tr("Zeige Figuren"));
    _viewBtn->setStatusTip("Zeichnet alle Figuren in ein neuen Fenster");
    _saveBtn = new QPushButton(tr("Speichern"));
    _saveBtn->setStatusTip("Speichert die 'Nested List' der Figuren in eine Datei");
    _quitBtn = new QPushButton(tr("Beenden"));
    _quitBtn->setStatusTip("Beendet die Anwendung");

    _figList = new QListWidget;
}

void FigureCreator::initSlotSignalConnection(){
     connect(_confirm, SIGNAL(clicked()), this , SLOT(createFigure()));
     connect(_delOne, SIGNAL(clicked()), this, SLOT(eraseFigure()));
     connect(_delAll, SIGNAL(clicked()), this, SLOT(clearList()));
     connect(_coordBtn, SIGNAL(clicked()), this, SLOT(createCoordViewer()));
     connect(_viewBtn, SIGNAL(clicked()), this, SLOT(createFigureViewer()));
     connect(_saveBtn, SIGNAL(clicked()), this, SLOT(saveCoordsToFile()));
     connect(_quitBtn, SIGNAL(clicked()), this, SLOT(close()));
}

void FigureCreator::initLayout(){

    /* Uebergeordnetes Layout */
    QVBoxLayout *layout = new QVBoxLayout;

    QGroupBox *upperbox = new QGroupBox(tr("Figuren"));
    QVBoxLayout *vupper = new QVBoxLayout;

    QHBoxLayout *radioLayout = new QHBoxLayout;
    radioLayout->addWidget(_lineBtn);
    radioLayout->addWidget(_circleBtn);
    radioLayout->addWidget(_triBtn);
    radioLayout->addWidget(_rectBtn);

    QHBoxLayout *genLayout = new QHBoxLayout;
    genLayout->addWidget(_amountTxt);
    genLayout->addWidget(_amount);
    genLayout->addWidget(_confirm);

    vupper->addLayout(radioLayout);
    vupper->addLayout(genLayout);

    upperbox->setLayout(vupper);

    QHBoxLayout *listTotalLayout = new QHBoxLayout;

    QVBoxLayout *listLayout = new QVBoxLayout;
    listLayout->addWidget(_figList);

    QVBoxLayout *listOpLayout = new QVBoxLayout;
    listOpLayout->addWidget(_delOne);
    listOpLayout->addWidget(_delAll);
    listOpLayout->addWidget(_coordBtn);
    listOpLayout->addWidget(_viewBtn);
    listOpLayout->addWidget(_saveBtn);
    listOpLayout->addWidget(_quitBtn);

    listTotalLayout->addLayout(listLayout);
    listTotalLayout->addLayout(listOpLayout);

    layout->addWidget(upperbox);
    layout->addLayout(genLayout);
    layout->addLayout(listTotalLayout);

    setLayout(layout);
    setWindowTitle(tr("Figurenverwalter[*]"));
    setWindowModified(true);
    show();
}

void FigureCreator::insertList(){
    _figList->clear();
    for(tNatZahl i = 0; i < _control->getFigureList().size(); i++){
        QString temp = QString::fromStdString(_control->getFigureList().at(i)->getFigureName());
        _figList->addItem(temp);
    }
    _control->getFigureList().clear();
}

void FigureCreator::createFigure() {
    QString txt = _amount->text();
    if( txt.isEmpty() || txt.size() > 2){
        QMessageBox::warning(this,tr("Fehler bei der Eingabe"),tr("Die Eingabe war groesser als 99 oder leer"));
        return;
    }

    try {
        tNatZahl number = txt.toUShort();

        if(_lineBtn->isChecked() ){
            _control->createFigure(number,'L');
        } else if ( _circleBtn->isChecked() ){
            _control->createFigure(number,'C');
        } else if ( _triBtn->isChecked() ){
            _control->createFigure(number,'T');
        } else if ( _rectBtn->isChecked() ){
            _control->createFigure(number,'R');
        }

        insertList();

    } catch (const bool &e){
         QMessageBox::critical(this,tr("Ungueltige Eingabe"),tr("Es wurde keine Zahl angegeben"));
    }
}

void FigureCreator::eraseFigure(){
    _control->deleteFigure(_figList->currentRow());
    delete _figList->currentItem();
}

void FigureCreator::clearList(){
    _figList->clear();
    _control->clearFigureList();
}

void FigureCreator::createCoordViewer(){
    new FigureData(_control);
}

void FigureCreator::createFigureViewer(){
    new FigurePainter(_control);
}

void FigureCreator::saveCoordsToFile(){
    QString filename = QFileDialog::getSaveFileName(
                this,
                tr("Figuren speichern"),
                ".txt",
                tr("Text Datei (*.txt);; Alle Dateien (*.*)"));

    if( filename.isEmpty() ){
        return;
    } else {
        QFile file(filename);
        if( file.open(QFile::WriteOnly | QFile::Text) ){
            QTextStream stream(&file);
            std::string *nl = _control->nestedListFromFigures();
            for(tNatZahl i = 0; i < _control->getFigureList().size(); i++){
                QString temp = QString::fromStdString(*(nl + i));
                stream << temp;
            }
            file.flush();
            file.close();
        } else {
            QMessageBox::information(this, tr("Konnte Datei nicht oeffnen"), file.errorString());
            return;
        }
    }
}

bool FigureCreator::closeWindow() {
    if( !isWindowModified() ){
        return true;
    }

    QMessageBox::StandardButton answer = QMessageBox::question(
                this,
                tr("Fenster schliessen"),
                tr("Soll das Fenster geschlossen werden?"),
                QMessageBox::Yes | QMessageBox::No);
    return (answer == QMessageBox::Yes);
}

void FigureCreator::closeEvent(QCloseEvent *event){
    if ( closeWindow() ){
        event->accept();
    } else {
        event->ignore();
    }
}
