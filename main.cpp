#include "VControler.h"
#include "Startwindow.h"
#include <QApplication>

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    VControler *control = new VControler();
    StartWindow *window = new StartWindow(control, nullptr);
    window->show();
    return a.exec();
}
