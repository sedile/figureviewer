#include "VTriangle.h"

VTriangle::VTriangle()
    : AbstractFigure(),
    _ll(VPoint()), _lr(VPoint()), _up(VPoint()) { }

VTriangle::VTriangle(const VPoint &ll, const VPoint &lr, const VPoint &up)
    : AbstractFigure(),
    _ll(ll), _lr(lr), _up(up) { }

VTriangle::VTriangle(const VTriangle &t)
    : AbstractFigure(),
    _ll(t._ll), _lr(t._lr), _up(t._up) { }

VTriangle::~VTriangle() {}

VTriangle* VTriangle::getTriangle() const {
    VTriangle *tri = new VTriangle(_ll,_lr,_up);
    return tri;
}

std::string VTriangle::getFigureName() const {
    return "VTriangle";
}

std::vector<tNatZahl> VTriangle::getCoords() const {
    std::vector<tNatZahl> coord;
    coord.push_back(_ll.getX());
    coord.push_back(_ll.getY());
    coord.push_back(_lr.getX());
    coord.push_back(_lr.getY());
    coord.push_back(_up.getX());
    coord.push_back(_up.getY());
    return coord;
}

std::string VTriangle::toString() const {
    return "( triangle(" + _ll.toString() + " " + _lr.toString() + " " + _up.toString() + ") )";
}
