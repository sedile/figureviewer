#include "FigureData.h"

#include <QVBoxLayout>

FigureData::FigureData(VControler *control, QWidget *parent) : QWidget(parent), _control(control) {
    initComponents();
    initSlotSignalConnection();
    initLayout();
    updateTextArea();
}

FigureData::~FigureData() {
    delete _figureTypes;
    delete _figureNL;
    delete _control;
}

void FigureData::initComponents(){
    _figureTypes = new QComboBox;
    _figureTypes->addItem("Alle");
    _figureTypes->addItem("Linie");
    _figureTypes->addItem("Kreis");
    _figureTypes->addItem("Dreieck");
    _figureTypes->addItem("Viereck");
    _figureTypes->setCurrentIndex(0);

    _figureNL = new QTextEdit;
    _figureNL->setFixedSize(480, 200);
    _figureNL->setReadOnly(true);
}

void FigureData::initSlotSignalConnection(){
    connect(_figureTypes, SIGNAL(currentIndexChanged(int)), this, SLOT(updateTextArea()));
}

void FigureData::initLayout(){
    QVBoxLayout *dataLayout = new QVBoxLayout;
    dataLayout->addWidget(_figureTypes);
    dataLayout->addWidget(_figureNL);

    QBoxLayout *layout = new QBoxLayout(QBoxLayout::TopToBottom);
    layout->addLayout(dataLayout);

    setLayout(layout);
    setWindowTitle("Koordinaten[*]");
    setWindowModality(Qt::ApplicationModal);
    show();

}

void FigureData::updateTextArea(){
    QStringList matchlist;
    for(tNatZahl i = 0; i < _control->getFigureList().size(); i++){
        if ( _figureTypes->currentIndex() == 0){
            QString temp = QString::fromStdString(_control->getFigureList().at(i)->toString());
            matchlist.append(temp);
        } else if ( _figureTypes->currentIndex() == 1){
            if( _control->getFigureList().at(i)->getFigureName() == "VLine"){
                QString temp = QString::fromStdString(_control->getFigureList().at(i)->toString());
                matchlist.append(temp);
            }
        } else if ( _figureTypes->currentIndex() == 2){
            if( _control->getFigureList().at(i)->getFigureName() == "VCircle"){
                QString temp = QString::fromStdString(_control->getFigureList().at(i)->toString());
                matchlist.append(temp);
            }
        } else if ( _figureTypes->currentIndex() == 3){
            if( _control->getFigureList().at(i)->getFigureName() == "VTriangle"){
                QString temp = QString::fromStdString(_control->getFigureList().at(i)->toString());
                matchlist.append(temp);
            }
        } else if ( _figureTypes->currentIndex() == 4){
            if( _control->getFigureList().at(i)->getFigureName() == "VRect"){
                QString temp = QString::fromStdString(_control->getFigureList().at(i)->toString());
                matchlist.append(temp);
            }
        }
    }

    _figureNL->clear();
    for(tNatZahl j = 0; j < matchlist.size(); j++){
        _figureNL->append(matchlist.at(j));
    }
}
