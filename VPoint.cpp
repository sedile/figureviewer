#include "VPoint.h"

VPoint::VPoint()
    : AbstractFigure() , _x(0), _y(0) { }

VPoint::VPoint(const tNatZahl x, const tNatZahl y)
    : AbstractFigure(), _x(x), _y(y) { }

VPoint::VPoint(const VPoint &p)
    : AbstractFigure(), _x(p.getX()), _y(p.getY()) { }

VPoint::~VPoint() {}

tNatZahl VPoint::getX() const {
    return _x;
}

tNatZahl VPoint::getY() const {
    return _y;
}

VPoint* VPoint::generateVPoints(const tNatZahl &ap){
    VPoint *vp = new VPoint[ap];
    for(tNatZahl i = 0; i < ap; i++){
        tNatZahl gx = rand() % 768;
        tNatZahl gy = rand() % 768;
        *(vp + i) = VPoint(gx,gy);
    }
    return vp;
}

std::string VPoint::getFigureName() const {
    return "VPoint";
}

std::vector<tNatZahl> VPoint::getCoords() const {
    std::vector<tNatZahl> coord;
    coord.push_back(_x);
    coord.push_back(_y);
    return coord;
}

std::string VPoint::toString() const {
    return "( point( " + std::to_string(_x) + " " + std::to_string(_y) + " ))";
}
