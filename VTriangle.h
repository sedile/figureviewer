#ifndef VTRIANGLE_H_INCLUDED
#define VTRIANGLE_H_INCLUDED

#include "AbstractFigure.h"
#include "VPoint.h"

class VTriangle : public AbstractFigure {
private:
    VPoint _ll, _lr, _up;
public:
    explicit VTriangle();
    explicit VTriangle(const VPoint &ll, const VPoint &lr, const VPoint &up);
    explicit VTriangle(const VTriangle &t);
    virtual ~VTriangle();

    VTriangle* getTriangle() const;

    std::string getFigureName() const override;
    std::vector<tNatZahl> getCoords() const override;
    std::string toString() const override;
};

#endif // VTRIANGLE_H_INCLUDED
