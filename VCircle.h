#ifndef VCIRCLE_H_INCLUDED
#define VCIRCLE_H_INCLUDED

#include "AbstractFigure.h"
#include "VPoint.h"

class VCircle : public AbstractFigure {
private:
    const VPoint _point;
    const tNatZahl _radius;
public:
    explicit VCircle();
    explicit VCircle(const VPoint &p, const tNatZahl &r);
    explicit VCircle(const VCircle &c);
    virtual ~VCircle();

    VCircle* getCircle() const;
    tNatZahl getRadius() const;

    std::string getFigureName() const override;
    std::vector<tNatZahl> getCoords() const override;
    std::string toString() const override;
};

#endif // VCIRCLE_H_INCLUDED
