#include "VLine.h"

VLine::VLine(const VPoint &start, const VPoint &end)
    : AbstractFigure(), _start(start), _end(end) { }

VLine::VLine(const VLine &l)
    : AbstractFigure(), _start(*l.getStartPoint()), _end(*l.getEndPoint()) {
    delete l.getStartPoint();
    delete l.getEndPoint();
}

VLine::~VLine() {}

VPoint* VLine::getStartPoint() const {
    return new VPoint(_start);
}

VPoint* VLine::getEndPoint() const {
    return new VPoint(_end);
}

std::string VLine::getFigureName() const {
    return "VLine";
}

std::vector<tNatZahl> VLine::getCoords() const {
    std::vector<tNatZahl> coord;
    coord.push_back(_start.getX());
    coord.push_back(_start.getY());
    coord.push_back(_end.getX());
    coord.push_back(_end.getY());;
    return coord;
}

std::string VLine::toString() const {
    return "( line(" + _start.toString() + " " + _end.toString() + ") )";
}
