#include "FigurePainter.h"

#include <QBoxLayout>

FigurePainter::FigurePainter(VControler *control, QWidget *parent) : QWidget(parent), _control(control) {
    initComponents();
    initSlotSignalConeection();
    initLayout();
    fillListWithFigures();
}

FigurePainter::~FigurePainter() {
    delete _figures;
    delete _drawFigure;
    delete _drawAll;
    delete _canvas;
    delete _control;
}

void FigurePainter::initComponents(){
    _figures = new QListWidget;

    _drawFigure = new QPushButton(tr("zeichne Figur"));
    _drawAll = new QPushButton(tr("zeichne alle"));

    _canvas = new Canvas;
    _canvas->setSingleFlag(false);
    _canvas->setFigureList(_control->getFigureList());
}

void FigurePainter::initSlotSignalConeection(){
    connect(_drawFigure, SIGNAL(clicked()), this, SLOT(drawOneFigure()));
    connect(_drawAll, SIGNAL(clicked()), this, SLOT(drawAllFigures()));
}

void FigurePainter::initLayout(){

    QHBoxLayout *upperleft = new QHBoxLayout;
    upperleft->addWidget(_figures);


    QVBoxLayout *upperright = new QVBoxLayout;
    upperright->addWidget(_drawFigure);
    upperright->addWidget(_drawAll);

    QVBoxLayout *canvasLayout = new QVBoxLayout;
    canvasLayout->addWidget(_canvas);

    QBoxLayout *upperlayout = new QBoxLayout(QBoxLayout::LeftToRight);
    upperlayout->addLayout(upperleft);
    upperlayout->addLayout(upperright);

    QBoxLayout *layout = new QBoxLayout(QBoxLayout::TopToBottom);
    layout->addLayout(upperlayout);
    layout->addLayout(canvasLayout);

    setLayout(layout);

    _canvas->setBackground();

    setWindowTitle("Figurenzeichner[*]");
    setWindowModality(Qt::ApplicationModal);
    show();
}

void FigurePainter::fillListWithFigures(){
    _figures->clear();
    for(tNatZahl i = 0; i < _control->getFigureList().size(); i++){
        QString temp = QString::fromStdString(_control->getFigureList().at(i)->toString());
        _figures->addItem(temp);
    }
}

void FigurePainter::drawOneFigure(){
    _canvas->setSingleFlag(true);
    _canvas->setFigure(_control->getFigure(_figures->currentRow()));
    _canvas->update();
}

void FigurePainter::drawAllFigures(){
    _canvas->setSingleFlag(false);
    _canvas->update();
}
