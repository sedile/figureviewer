#ifndef VRECT_H_INCLUDED
#define VRECT_H_INCLUDED

#include "AbstractFigure.h"
#include "VRect.h"
#include "VPoint.h"

class VRect : public AbstractFigure {
private:
    VPoint _ul, _ur, _ll, _lr;
public:
    explicit VRect();
    explicit VRect(const VPoint &ul, const VPoint &ur, const VPoint &ll, const VPoint &lr);
    explicit VRect(const VRect &r);
    virtual ~VRect();

    VRect* getRect() const;

    std::string getFigureName() const override;
    std::vector<tNatZahl> getCoords() const override;
    std::string toString() const override;
};

#endif // VRECT_H_INCLUDED
