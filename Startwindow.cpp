#include "Startwindow.h"
#include "FigureCreator.h"
#include "FigurePainter.h"
#include "FigureData.h"

StartWindow::StartWindow(VControler *control, QWidget *parent) : QMainWindow(parent), _control(control){
    initComponents();
    initConnections();
    initLayout();
}

StartWindow::~StartWindow(){
    delete _control;
}

void StartWindow::initComponents(){
    _windowMenu = new QMenu(tr("&Fenster"));
    _close = new QAction(tr("&Beenden"), this);
    _close->setStatusTip("Beendet die Anwendung");

    _chooserMenu = new QMenu(tr("&Figuren"));
    _painter = new QAction(tr("Zeichnen"), this);
    _painter->setStatusTip("Lasse Figuren zeichnen");
    _coord = new QAction(tr("Koordinaten"), this);
    _coord->setStatusTip("Lasse die Koordinaten der Figuren anzeigen");

    _chooserMenu->addAction(_painter);
    _chooserMenu->addAction(_coord);

    _windowMenu->addMenu(_chooserMenu);
    _windowMenu->addSeparator();
    _windowMenu->addAction(_close);

    _tb = addToolBar("Test");
    _tb->addAction(_close);

    menuBar()->addMenu(_windowMenu);
    statusBar()->showMessage("Statusbarnachricht", 3000);

    _figurecreator = new FigureCreator(_control);
}

void StartWindow::initLayout(){
    setCentralWidget(_figurecreator);
    setWindowTitle("FigureViewer (Test)[*]");
    setFixedSize(640,480);
    show();
}

void StartWindow::initConnections(){
    connect(_painter, SIGNAL(triggered()), this, SLOT(openPainter()));
    connect(_coord, SIGNAL(triggered()), this, SLOT(openCoord()));
    connect(_close, SIGNAL(triggered()), this, SLOT(closeApp()));
}

void StartWindow::openPainter(){
    new FigurePainter(_control);
}

void StartWindow::openCoord(){
    new FigureData(_control);
}

void StartWindow::closeApp(){
    close();
}
