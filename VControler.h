#ifndef VCONTROLER_H_INCLUDED
#define VCONTROLER_H_INCLUDED

#include <vector>
#include <string>
#include "AbstractFigure.h"

class VControler {
private:
    std::vector<AbstractFigure*> _figurelist;

    std::vector<std::string> extractPoints(const std::string &figure);

public:
    explicit VControler();
    virtual ~VControler();

    /* eine Figur durch Eingabe erzeugen und bestÃ¤tigung zurÃ¼ckgebebn
       => In der GUI ein DropDown und User gibt je nach Figur eine vorgegebene
       Anzahl an Parameter an */
    bool createFigure(const tNatZahl &length, const char &name);

    /* eine Figur nach ausgewÃ¤hlten Index lÃ¶schen, und bestÃ¤tigen.
       => In der GUI wird aus einer Auswahlliste der Index der Figur bestimmt */
    bool deleteFigure(const tNatZahl &idx);

    /* alle Figuren lÃ¶schen
       => In der GUI wird ein Button gedrÃ¼ckt und auf Nachfrage die Liste gelÃ¶scht */
    void clearFigureList();

    /* Punkte aus einzelnen Figuren als ListView darstellen
       => In der GUI wird aus einer Liste der Index bestimmt und anhand der Figur
       die innere Liste erstellt und zurÃ¼ckgegeben */
    std::vector<std::string> showPointsFromFigure(const tNatZahl &idx);

    /* Figur aus Liste wÃ¤hlen und zeichnen (Koordinaten erhalten)
       => In der GUI wird eine Figur aus Liste gewÃ¤hlt und anhand dessen die Coord bestimmt */
    AbstractFigure* getFigure(const tNatZahl &idx) const;

    /* Alle Figuren aus der Liste zeichnen
       => In der GUI wird ein Button gedrÃ¼ckt, um alle Figuren zu zeichnen */
    std::vector<AbstractFigure*> getFigureList() const;
    /* Alle Figuren als 'NestedList' darstellen in ein Fenster
       => In der GUI wird ein neues Fenster aufgerufen (Modal) und NL angezeigt */
    std::string* nestedListFromFigures() const;
};

#endif // VCONTROLER_H_INCLUDED
