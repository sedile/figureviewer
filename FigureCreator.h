#ifndef FIGURECREATOR_H
#define FIGURECREATOR_H

#include <QRadioButton>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QListWidget>
#include <QCloseEvent>

#include "VControler.h"

class FigureCreator : public QWidget {
private:
    Q_OBJECT

    VControler *_control;
    QRadioButton *_lineBtn, *_circleBtn, *_triBtn, *_rectBtn;
    QLabel *_amountTxt;
    QLineEdit *_amount;
    QPushButton *_confirm, *_delOne, *_delAll, *_coordBtn, *_viewBtn, *_saveBtn, *_quitBtn;
    QListWidget *_figList;

    void initComponents();
    void initSlotSignalConnection();
    void initLayout();
    void insertList();

public:
    explicit FigureCreator(VControler *control, QWidget *parent = nullptr);
    virtual ~FigureCreator();

protected:
    void closeEvent(QCloseEvent *event);

private slots:
    void createFigure();
    void eraseFigure();
    void clearList();
    void createCoordViewer();
    void createFigureViewer();
    void saveCoordsToFile();
    bool closeWindow();
};

#endif // FIGURECREATOR_H
